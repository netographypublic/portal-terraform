##################################
# Locals & Data Sources
##################################

locals {
  resource_credentials = jsondecode(ibm_resource_key.key.credentials_json)
}

data "ibm_resource_group" "this" {
  name = var.ibm_resource_group
}

data "ibm_is_vpc" "this" {
  name = var.vpc_name
}


##################################
# Object storage &
# Log Objects Handling
##################################

resource "ibm_resource_instance" "this" {
  name              = "${var.vpc_name}-flowlogs-instance"
  resource_group_id = data.ibm_resource_group.this.id
  service           = "cloud-object-storage"
  plan              = var.storage_class
  location          = "global"
}

resource "ibm_cos_bucket" "this" {
  bucket_name          = "${var.vpc_name}-${var.bucket_name}"
  resource_instance_id = ibm_resource_instance.this.id
  region_location      = var.region
  storage_class        = var.storage_class
  force_delete         = var.force_delete

  object_versioning {
    enable = true
  }
  expire_rule {
    rule_id                      = "${var.vpc_name}-flowlogs-object-expiry-rule"
    enable                       = true
    expired_object_delete_marker = true
  }
  noncurrent_version_expiration {
    rule_id         = "${var.vpc_name}-flowlogs-object-expiry-version"
    enable          = true
    prefix          = ""
    noncurrent_days = 1
  }
  timeouts {
    create = "5m"
    delete = "5m"
  }
}

##################################
# Create api-key and resource crn
##################################

resource "ibm_resource_key" "key" {
  name                 = "${var.vpc_name}-flowlogs-service-credential"
  role                 = "Reader"
  resource_instance_id = ibm_resource_instance.this.id

  //User can increase timeouts
  timeouts {
    create = "5m"
    delete = "5m"
  }
}


########################################
# Create IAM Authorization btw VPC - 
# - flowlogs and Object Storage
#######################################

resource "ibm_iam_authorization_policy" "policy" {
  source_service_name         = "is"
  source_resource_type        = "flow-log-collector"
  target_service_name         = "cloud-object-storage"
  target_resource_instance_id = ibm_resource_instance.this.guid
  roles                       = ["Writer"]
}


##################################
# Create Flow Log
################################## 

resource "ibm_is_flow_log" "this" {
  depends_on     = [ibm_cos_bucket.this]
  name           = "${var.vpc_name}-flow-log"
  target         = data.ibm_is_vpc.this.id
  active         = true
  storage_bucket = ibm_cos_bucket.this.bucket_name
  resource_group = data.ibm_resource_group.this.id
}
