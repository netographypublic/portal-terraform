variable "region" {
  type        = string
  description = "Oracle region for resource deployment"
  default     = ""
}

variable "vpc_name" {
  description = "Name of Virtual Private Cloud where logs are generated"
  type        = string
  default     = ""
}

variable "bucket_name" {
  type        = string
  description = "COS Bucket name"
  default     = ""
}


variable "ibm_resource_group" {
  description = "Name of Resource Group in IBM Cloud  "
  type        = string
  default     = ""
}

variable "storage_class" {
  description = " storage class to use for the bucket."
  type        = string
  default     = "standard"
}

variable "endpoint_type" {
  description = "endpoint for the COS bucket"
  type        = string
  default     = "private"
}


variable "force_delete" {
  description = "COS buckets need to be empty before they can be deleted. force_delete option empty the bucket and delete it"
  type        = bool
  default     = true
}
