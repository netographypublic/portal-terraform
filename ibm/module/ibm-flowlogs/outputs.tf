output "credentials" {
  value = local.resource_credentials
}

output "bucket_name" {
  value = ibm_cos_bucket.this.bucket_name
}