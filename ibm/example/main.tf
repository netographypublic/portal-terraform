####################################################
# Create Resources to enable flowlog collection
# Will create Policy, Bucket, Service Instance &
# Flow Log
####################################################

module "flowlogs" {
  source             = "../module/ibm-flowlogs"
  region             = var.region
  vpc_name           = var.vpc_name
  ibm_resource_group = var.ibm_resource_group
  bucket_name        = var.bucket_name
}


############################################################
# Authenticate to the Netography API via local -
# - shell to get a bearer token
############################################################

resource "null_resource" "get_bearer_token" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_token.py"
    environment = {
      CUSTOMER_USERNAME = var.customer_username
      CUSTOMER_PASSWORD = var.customer_password
    }
  }
}

###########################################################
# Send POST request using token to Netography Portal API, - 
# -  to add cloud provider for flow collection
###########################################################

data "http" "api_call" {
  depends_on = [
    module.flowlogs
  ]

  provider = http-full
  url      = "https://api-staging.netography.com/api/v1/vpc"
  method   = "POST"
  request_headers = {
    content-type  = "application/json"
    Authorization = "Bearer ${chomp(file("scripts/bearer_token.txt"))}"
  }
  request_body = jsonencode(
    {
      "name" : "${var.portal_cloud_provider_name}",
      "enabled" : true,
      "samplerate" : 1,
      "flowtype" : "ibm",
      "flowresource" : "cos",
      "region" : "${var.region}",
      "apikey" : "${module.flowlogs.credentials.apikey}",
      "bucket" : "${module.flowlogs.bucket_name}",
      "serviceinstanceid" : "${module.flowlogs.credentials.resource_instance_id}",
      "prefix" : ""
    }
  )
}