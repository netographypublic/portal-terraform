variable "ibmcloud_api_key" {
  type        = string
  description = "API key for connecting tyo IBM Cloud"
}

variable "region" {
  type        = string
  description = "IBM region for resource deployment"
}

variable "vpc_name" {
  description = "Name of Virtual Private Cloud where logs are generated"
  type        = string
}

variable "bucket_name" {
  type        = string
  description = "COS Bucket name"
}

variable "ibm_resource_group" {
  description = "Name of Resource Group in IBM Cloud  "
  type        = string
}

variable "portal_cloud_provider_name" {
  type        = string
  description = "Name of the cloud provider account to add to the Netography portal"
}

variable "customer_username" {
  type        = string
  description = "the authenticated username of the Netography portal customer"
}

variable "customer_password" {
  type        = string
  description = "the authenticated password of the Netography portal customer"
}