output "flowlogs" {
  description = "The response body of the API call to Netography API"
  value       = module.flowlogs
  sensitive = true
}

output "data_json" {
  description = "The response body of the API call to Netography API"
  value       = data.http.api_call.body
}

