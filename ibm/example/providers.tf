##########################
# PROVIDER CONFIGURATION
##########################

provider "ibm" {
  ibmcloud_api_key = var.ibmcloud_api_key
  region           = var.region
}

provider "http-full" {}


##########################
# TERRAFORM CONFIGURATION
##########################

terraform {
  required_providers {
    ibm = {
      source = "ibm-cloud/ibm"
    }
    http-full = {
      source  = "salrashid123/http-full"
      version = "1.2.3"
    }
  }
  required_version = ">= 0.13"
}