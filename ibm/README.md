# Netography Portal Terraform IBM Integration

Terraform module for Netography Portal Integration.
Enables customers using terraform to directly provision resources for flowlog collection in their IBM cloud environment and add required cloud provider configuration to the Netography portal in one step.

## Usage

```hcl
module "flowlogs" {
  source             = "../module/ibm-flowlogs"
  region             = var.region
  vpc_name           = var.vpc_name
  ibm_resource_group = var.ibm_resource_group
  bucket_name        = var.bucket_name
}
```

## Example

- [Provision Flowlog resources and Authenticate via Netography API to add cloud provider](https://gitlab.com/netographypublic/portal-terraform/-/tree/main/ibm/example)

## Requirements

| Name | Version |
|------|---------|
| <a name="x"></a> [terraform](#requirement\_terraform) | >= 1.1.6 |
| <a name="x"></a> [ibm](#requirement\_oracle_) | >= 1.41.0 |
| <a name="x"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_python) | >= 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [ibm](#provider\_oracle) | >= 1.41.0 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |

## Resources Created

| Name | Type |
|------|------|
| [ibm_resource_instance.this](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest/docs/resources/resource_instance) | resource |
| [ibm_iam_authorization_policy.policy](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest/docs/resources/ibm_iam_authorization_policy) | resource |
| [ibm_resource_key.key](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest/docs/resources/ibm_resource_key) | resource |
| [ibm_cos_bucket.this](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest/docs/resources/ibm_cos_bucket) | resource |
| [ibm_is_flow_log.this](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest/docs/resources/ibm_is_flow_log) | resource |

## Inputs

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| <a name="x"></a> [region](#x) | The IBM region of the VPC | `string` | yes |
| <a name="x"></a> [vpc\_name](#x) | The name of the IBM VPC to be be logged | `string` | yes |
| <a name="x"></a> [resource\_group](#x) | The IBM resource group where resources will be created | `string` | yes |
| <a name="x"></a> [bucket\_name](#x) |The name of the IBM Object storage to be created | `string` | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="x"></a> [credentials](#x) | The api credentials for the service instance creates |
| <a name="x"></a> [bucket\_name](#x) | The name of the IBM Object storage created |
| <a name="x"></a> [data\_json](#x) | The response of the HTTP POST Request in JSON format |

## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
