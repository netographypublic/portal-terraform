# Netography Portal Terraform Integration

Terraform module for Netography Portal Integration. 
Enables customers using terraform to directly provision resources for flowlog collection in their respective cloud environments and add required cloud provider configuration to the Netography portal in one step.

## Usage


* Open the folder named after your respective cloud provider.
* Create a `terraform.tfvars` file and input in the required variables and save file.
* run the terraform commands `validate`, `plan`, and `apply` to provision and add cloud provider to Netography portal 
* Verify the `200` HTTP response code in the `data_json` output.  


## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.1 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_http) | >= 3.6 |

**Note: Additional requirements outlined in README of respective cloud folders**


## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
