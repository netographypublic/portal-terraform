##########################
# PROVIDER CONFIGURATION
##########################

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

provider "http-full" {}

##########################
# TERRAFORM CONFIGURATION
##########################

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.0.2"
    }
    http-full = {
      source  = "salrashid123/http-full"
      version = "1.2.3"
    }
  }
  required_version = ">= 1.1.0"
}
