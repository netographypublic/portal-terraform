variable "subscription_id" {
  type        = string
  description = "The Azure Network Security Group's subscription ID"
}

variable "resource_group_name" {
  description = "A container that holds related resources for an Azure solution"
  type        = string
}

variable "storage_account_name" {
  description = "Storage account name"
  type        = string
}

variable "vnetwork_name" {
  description = "Name of your Azure Virtual Network"
  type        = string
}

variable "flowlogs_name" {
  description = "Name of your Azure Virtual Network Flow Logs"
  type        = string
}

variable "flowlogs_analytics_name" {
  description = "Name of your Azure Virtual Network Flow Logs Analytics Service to be created"
  type        = string
  default     = "flowlogs-analytics"
}

variable "network_security_group_name" {
  description = "Name of the Azure Network Security Group to be created"
  type        = string
  default     = "flowlogs-nsg"
}

variable "network_watcher_name" {
  description = "Name of the Azure Network Watcher to be created "
  type        = string
  default     = "flowlogs-network-watcher"
}

variable "tags" {
  description = "Tags to add to all resources created"
  type        = map(string)
}

variable "portal_cloud_provider_name" {
  type        = string
  description = "Name of the cloud provider account to add to the Netography portal"
}

variable "customer_username" {
  type        = string
  description = "the auntheticated username of the Netography portal customer"
}

variable "customer_password" {
  type        = string
  description = "the auntheticated password of the Netography portal customer"
}
