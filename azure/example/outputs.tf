output "flowlogs" {
  description = "All outputs of the flowlog module, include; bucket_arn, bucket_name, sqs_url, bucket_region, iam_user access_key & secret_key"
  sensitive   = true
  value       = module.flowlogs
}

output "data_json" {
  description = "The response body of the API call to Netography API"
  value       = data.http.api_call.body
}
