####################################################
# Create Resources to enable flowlog collection
####################################################

module "flowlogs" {
  source = "../modules/nsg-flowlogs"

  resource_group_name         = var.resource_group_name
  vnetwork_name               = var.vnetwork_name
  storage_account_name        = var.storage_account_name
  network_watcher_name        = var.network_watcher_name
  network_security_group_name = var.network_security_group_name
  flowlogs_name               = var.flowlogs_name
  # flowlogs_analytics_name     = var.flowlogs_analytics_name     #Optional, NOTE: if specified,module will auto-add vnetwork prefix
  tags = var.tags
}

############################################################
# Authenticate to the Netography API via local -
# - shell to get a bearer token
############################################################

resource "null_resource" "get_bearer_token" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_token.py"
    environment = {
      CUSTOMER_USERNAME = var.customer_username
      CUSTOMER_PASSWORD = var.customer_password
    }
  }
}

############################################################
# Send POST request using bearer token to Netography API, - 
# -  add cloud provider to portal for flow collection
############################################################

data "http" "api_call" {
  depends_on = [
    module.flowlogs
  ]

  provider = http-full
  url      = "https://api.netography.com/api/v1/vpc"
  method   = "POST"
  request_headers = {
    content-type  = "application/json"
    Authorization = "Bearer ${chomp(file("scripts/bearer_token.txt"))}"
  }
  request_body = jsonencode(
    {
      "name" : "${var.portal_cloud_provider_name}",
      "enabled" : true,
      "samplerate" : 1,
      "flowtype" : "azure",
      "flowresource" : "nsg",
      "region" : "${module.flowlogs.region}",
      "accountname" : "${module.flowlogs.storage_account}",
      "accountkey" : "${module.flowlogs.access_key}",
      "containername" : "insights-logs-networksecuritygroupflowevent",
      "resourcegroup" : "${module.flowlogs.resource_group}",
      "subscriptionid" : "${var.subscription_id}",
      "networksecuritygroup" : "${module.flowlogs.network_security_group}",
    }
  )
}
