#-----------------------------------------------
# Defining locals &
# Data Sources 
#-----------------------------------------------

locals {
  resource_group = data.azurerm_resource_group.this
}

data "azurerm_resource_group" "this" {
  name  = var.resource_group_name
}

data "azurerm_network_security_group" "this" {
  name                = var.network_security_group_name
  resource_group_name = local.resource_group.name
}

data "azurerm_network_watcher" "this" {
  name                = var.network_watcher_name
  resource_group_name = local.resource_group.name
}

#-----------------------------
# Storage Account
#-----------------------------

resource "azurerm_storage_account" "this" {
  name                = var.storage_account_name
  location            = local.resource_group.location
  resource_group_name = local.resource_group.name

  account_tier              = "Standard"
  account_kind              = "StorageV2"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
  tags                      = var.tags
}


#-----------------------------------------------
# Azure Network Flow-Logs & Analytics
#-----------------------------------------------

resource "azurerm_log_analytics_workspace" "this" {
  name                = "${var.vnetwork_name}-${var.flowlogs_analytics_name}"
  location            = local.resource_group.location
  resource_group_name = local.resource_group.name
  sku                 = "PerGB2018"
}

resource "azurerm_network_watcher_flow_log" "this" {
  network_watcher_name = data.azurerm_network_watcher.this.name
  resource_group_name  = local.resource_group.name
  name                 = "${var.vnetwork_name}-${var.flowlogs_name}"

  network_security_group_id = data.azurerm_network_security_group.this.id
  storage_account_id        = azurerm_storage_account.this.id
  enabled                   = true
  version = 2

  retention_policy {
    enabled = true
    days    = 1
  }

  traffic_analytics {
    enabled               = true
    workspace_id          = azurerm_log_analytics_workspace.this.workspace_id
    workspace_region      = azurerm_log_analytics_workspace.this.location
    workspace_resource_id = azurerm_log_analytics_workspace.this.id
    interval_in_minutes   = 10
  }
  tags                    = var.tags
}
