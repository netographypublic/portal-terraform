output "region" {
  description = "The region of the resource group in which resources are created"
  value       = data.azurerm_resource_group.this.location
}

output "resource_group" {
  description = "The name of the resource group in which resources are created"
  value       = data.azurerm_resource_group.this.name
}

output "network_security_group" {
  description = "Name of the Network security groups"
  value       = data.azurerm_network_security_group.this.name
}

output "flowlog_name" {
  description = "ID of Network Watcher"
  value       = azurerm_network_watcher_flow_log.this.name
}

output "storage_account" {
  description = "ID of Network Watcher"
  value       = azurerm_storage_account.this.name
}

output "access_key" {
  description = "Primary Access Key for the Storage Account"
  value       = azurerm_storage_account.this.primary_access_key
}
