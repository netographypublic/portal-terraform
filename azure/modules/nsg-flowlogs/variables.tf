variable "resource_group_name" {
  description = "A container that holds related resources for an Azure solution"
  type        = string
  default     = ""
}

variable "storage_account_name" {
  description = "Storage account name"
  type        = string
  default     = ""
}

variable "vnetwork_name" {
  description = "Name of your Azure Virtual Network"
  type        = string
  default     = ""
}

variable "flowlogs_name" {
  description = "Name of your Azure Virtual Network Flow Logs"
  type        = string
  default     = "flowlogs"
}

variable "flowlogs_analytics_name" {
  description = "Name of your Azure Virtual Network Flow Logs Analytics Service to be created"
  type        = string
  default     = "flowlogs-analytics"
}

variable "network_security_group_name" {
  description = "Name of the Azure Network Security Group to be created"
  type        = string
  default     = ""
}

variable "network_watcher_name" {
  description = "Name of the Azure Network Watcher to be created "
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
