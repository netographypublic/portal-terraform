# Netography Portal Terraform Azure Integration

Terraform module for Netography Portal Integration.
Enables customers using terraform to directly provision resources for flowlog collection in their Azure cloud environment and add required cloud provider configuration to the Netography portal in one step.

## Usage

```hcl
module "flowlogs" {
  source = "../modules/nsg-flowlogs"
  resource_group_name         = var.resource_group_name
  vnetwork_name               = var.vnetwork_name
  storage_account_name        = var.storage_account_name
  network_watcher_name        = var.network_watcher_name
  network_security_group_name = var.network_security_group_name
  flowlogs_name               = var.flowlogs_name
  tags = var.tags
}
```

## Example

- [Provision Flowlog resources and Authenticate via Netography API to add cloud provider](https://gitlab.com/netographypublic/portal-terraform/-/tree/main/azure/example)

## Requirements

| Name | Version |
|------|---------|
| <a name="x"></a> [terraform](#requirement\_terraform) | >= 1.1.6 |
| <a name="x"></a> [azure](#requirement\_azure_) | >= 3.0.2 |
| <a name="x"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_python) | >= 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [azure](#provider\_azure) | >= 3.0.2 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |

## Resources Created

| Name | Type |
|------|------|
| [azurerm_storage_account.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account/ibm_is_flow_log) | resource |
| [azurerm_log_analytics_workspace.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/log_analytics_workspace) | resource |
| [azurerm_network_watcher_flow_log.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_watcher_flow_log) | resource |

## Inputs

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| <a name="x"></a> [resource_group_name](#x) | The Azure Resource Group where resources will be created | `string` | yes |
| <a name="x"></a> [vnetwork_name](#x) | Name of the Azure Virtual Network where subnets reside | `string` | yes |
| <a name="x"></a> [storage_account_name](#x) | Name of the Azure Storage Account to be created | `string` | yes |
| <a name="x"></a> [network_security_group_name](#x) | The Azure Network Security Group to be data sourced for use | `string` | yes |
| <a name="x"></a> [network_watcher_name](#x) | The Azure Network Watcher to be data sourced for use | `string` | yes |
| <a name="x"></a> [flowlogs_name](#x) | Name of the Azure Network Watcher Flow Log to be created | `string` | yes |
| <a name="x"></a> [tags](#x) | Tags added to all created resources | `string` | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="x"></a> [region](#x) | The regional location of the Azure Resource group |
| <a name="x"></a> [resource\_group](#x) | Name of the Azure Resource Group |
| <a name="x"></a> [network\_security\_group](#x) | Name of the Azure Network Security Group |
| <a name="x"></a> [flowlog\_name](#x) | Name of the Azure Network Watcher Flow Log |
| <a name="x"></a> [storage\_account](#x) | Name of the Azure Storage Account |
| <a name="x"></a> [access\_key](#x) | The Primary Access Key for the Storage Account |
| <a name="x"></a> [data\_json](#x) | The response of the HTTP POST Request in JSON format |

## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
