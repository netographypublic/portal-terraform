output "subscription_id" {
  description = "The ID of the Pub/Sub subscription that will be created"
  value       = google_pubsub_subscription.this.name
}