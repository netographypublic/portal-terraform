variable "pubsub_topic_name" {
  description = "Pub/Sub topic name"
  type        = string
  default     = ""
}

variable "pubsub_subscription_name" {
  description = "Pub/Sub subscription name"
  type        = string
  default     = ""
}

variable "logging_sink_name" {
  description = "Google Logging Project Sink name"
  type        = string
  default     = ""
}

variable "project_name" {
  description = "Name of GCP project"
  type        = string
  default     = ""
}

variable "pubsub_subscription_member" {
  description = "Service account to grant IAM role to access Pub/Sub subscription"
  type        = string
  default     = "sa-cloud@netography.iam.gserviceaccount.com"
}