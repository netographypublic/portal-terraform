##################################################
# Google Pub-Sub Topic
##################################################

resource "google_pubsub_topic" "this" {
  name = "${var.project_name}-${var.pubsub_topic_name}"
}

####################################################
# Google Pub-Sub Subscription
####################################################

resource "google_pubsub_subscription" "this" {
  name  = "${var.project_name}-${var.pubsub_subscription_name}"
  topic = google_pubsub_topic.this.name

  # 1 day retention
  message_retention_duration = "3600s"
  ack_deadline_seconds = 20

   expiration_policy {
    ttl = "86400s"
  }
}

resource "google_pubsub_subscription_iam_member" "this" {
  subscription = google_pubsub_subscription.this.name
  role         = "roles/pubsub.subscriber"
  member       = "serviceAccount:${var.pubsub_subscription_member}"
}


############################################
# Flow Log Sink
############################################

resource "google_logging_project_sink" "this" {
  depends_on = [
    google_pubsub_topic.this
  ]

  name = "${var.project_name}-${var.logging_sink_name}"
  destination = "pubsub.googleapis.com/projects/${var.project_name}/topics/${google_pubsub_topic.this.name}"
  unique_writer_identity = true
}

# Because our sink uses a unique_writer, we must grant that writer access to the bucket.
resource "google_project_iam_binding" "log-writer" {
  depends_on = [
    google_logging_project_sink.this
  ]

  project = google_pubsub_topic.this.project
  role = "roles/pubsub.publisher"
  members = [
    google_logging_project_sink.this.writer_identity,
  ]
}