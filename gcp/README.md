# Netography Portal Terraform GCP Integration

Terraform module for Netography Portal Integration. 
Enables customers using terraform to directly provision resources for flowlog collection in their GCP cloud environment and add required cloud provider configuration to the Netography portal in one step.

## Usage

```hcl
module "flowlog" {
  source                   = "../modules/flowlogs-pubsub"
  project_name             = var.project_name
  pubsub_topic_name        = var.pubsub_topic_name
```

## Example

- [Provision Flowlog resources and Authenticate via Netography API to add cloud provider](https://gitlab.com/netographypublic/portal-terraform/-/tree/main/gcp/example)


## Requirements

| Name | Version |
|------|---------|
| <a name="x"></a> [terraform](#requirement\_terraform) | >= 1.1.6 |
| <a name="x"></a> [google](#requirement\_google) | >= 4.0 |
| <a name="x"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_python) | >= 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |

## Resources Created

| Name | Type |
|------|------|
| [google_project_iam_binding.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_project_iam) | resource |
| [google_pubsub_topic.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_pubsub_topic) | resource |
| [google_pubsub_subscription.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_pubsub_subscription) | resource |
| [google_pubsub_subscription_iam_member.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/pubsub_subscription_iam) | resource |
| [google_logging_project_sink.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_logging_project_sink) | resource |

## Inputs

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| <a name="x"></a> [project\_name](#x) | The name of the GCP project to be worked on | `string` | yes |
| <a name="x"></a> [pubsub\_topic\_name](#x) | The name of the Google Pub/Sub Topic to be created | `string` | yes |
| <a name="x"></a> [pubsub\_subscription\_name](#x) | The name of the Google Pub/Sub Subcription to be created | `string` | no |
| <a name="x"></a> [logging\_sink\_name](#x) |The name of the Google Logging Project Sink to be created | `string` | no |



## Outputs

| Name | Description |
|------|-------------|
| <a name="x"></a> [subscription\_id](#x) | The name of the Google Pub/Sub Subcription created |
| <a name="x"></a> [data\_json](#x) | The response of the HTTP POST Request in JSON format |


## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
