variable "region" {
  type        = string
  description = "GCP region for resource deployment"
}

variable "project_id" {
  type        = string
  description = "The ID of the project where provisioned resources exist in"
}

variable "project_name" {
  type        = string
  description = "The name of the project where  provisioned resources exist in"
}

variable "pubsub_topic_name" {
  type        = string
  description = "Name of the PubSub topic to be created"
  default     = "flow-log-collection-topic"
}

variable "logging_sink_name" {
  description = "Google Logging Project Sink name"
  type        = string
  default     = "flow-log-collection-sink"
}

variable "pubsub_subscription_name" {
  description = "Pub/Sub subscription name"
  type        = string
  default     = "flow-log-collection-subscription"
}

variable "portal_cloud_provider_name" {
  type        = string
  description = "Name of the cloud provider account to add to the Netography portal"
}

variable "customer_username" {
  type        = string
  description = "the authenticated username of the Netography portal customer"
}

variable "customer_password" {
  type        = string
  description = "the authenticated password of the Netography portal customer"
}
