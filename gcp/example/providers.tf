##########################
# PROVIDER CONFIGURATION
##########################
provider "google" {
  project = var.project_id
  region  = var.region
}

provider "http-full" {}

##########################
# TERRAFORM CONFIGURATION

##########################

terraform {
  required_version = ">=1.1.6"

  required_providers {
    google = {
      version = "~> 4.0"
    }
    http-full = {
      source  = "salrashid123/http-full"
      version = "1.2.3"
    }
  }
}
