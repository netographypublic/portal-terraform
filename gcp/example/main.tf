####################################################
# Create Resources to enable flowlog collection
# Will create topic, subscription, sink
####################################################

module "flowlog" {
  source                   = "../modules/flowlogs-pubsub"
  project_name             = var.project_name
  pubsub_topic_name        = var.pubsub_topic_name
  pubsub_subscription_name = var.pubsub_subscription_name #Optional, NOTE: if specified, module will auto-add project-name prefix
  logging_sink_name        = var.logging_sink_name        #Optional, NOTE: if specified, module will auto-add project-name prefix
}


############################################################
# Authenticate to the Netography API via local -
# - shell to get a bearer token
############################################################

resource "null_resource" "get_bearer_token" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_token.py"
    environment = {
      CUSTOMER_USERNAME = var.customer_username
      CUSTOMER_PASSWORD = var.customer_password
    }
  }
}


############################################################
# Send POST request using bearer token to Netography API, - 
# -  add cloud provider to portal for flow collection
############################################################

data "http" "api_call" {
  depends_on = [
    module.flowlog
  ]

  provider = http-full
  url      = "https://api.netography.com/api/v1/vpc"
  method   = "POST"
  request_headers = {
    content-type  = "application/json"
    Authorization = "Bearer ${chomp(file("scripts/bearer_token.txt"))}"
  }
  request_body = jsonencode(
    {
      "name" : "${var.portal_cloud_provider_name}",
      "enabled" : true,
      "samplerate" : 1,
      "flowtype" : "gcp",
      "flowresource" : "pubsub",
      "region" : "${var.region}",
      "projectid" : "${var.project_id}",
      "subid" : "${module.flowlog.subscription_id}",
      "samplepercent" : 100
    }
  )
}