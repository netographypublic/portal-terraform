locals {
  oci_log_ids = var.oci_log_ids
}


##################################
# Create User 
##################################
resource "oci_identity_user" "this" {
  name           = "${var.vcn_name}-flowlog-user"
  description    = "User created to collect ${var.vcn_name}-vcn-flowlogs"
  compartment_id = var.tenancy_ocid
}

resource "oci_identity_user_capabilities_management" "this" {
  user_id                      = oci_identity_user.this.id
  can_use_api_keys             = "true"
  can_use_auth_tokens          = "false"
  can_use_console_password     = "false"
  can_use_customer_secret_keys = "false"
  can_use_smtp_credentials     = "false"
}

resource "oci_identity_api_key" "this" {
  user_id = oci_identity_user.this.id
  key_value = var.pub_key_value
}


##################################
# Create Group & Add User
##################################

resource "oci_identity_group" "this" {
  name           = "${var.vcn_name}-flow-logs"
  description    = "Users in this group can access ${var.vcn_name} flowlogs"
  compartment_id = var.tenancy_ocid
}

resource "oci_identity_user_group_membership" "this" {
  compartment_id = var.tenancy_ocid
  user_id        = oci_identity_user.this.id
  group_id       = oci_identity_group.this.id
}


##################################
# Create Group Policy and - 
# - Service Connector Policy
##################################

resource "oci_identity_policy" "group" {
  name           = "${var.vcn_name}-flowlogs-policy"
  description    = "Allows access to logs in object storage, allow lifecycle to remove old logs, and allow read access for VNIC and Instances"
  compartment_id = var.tenancy_ocid
  statements = [
    "Allow group ${oci_identity_group.this.name} to read instances in tenancy",
    "Allow group ${oci_identity_group.this.name} to read virtual-network-family in tenancy",
    "Allow group ${oci_identity_group.this.name} to read instance-family in tenancy",
    "Allow group ${oci_identity_group.this.name} to read object-family in tenancy",
    "Allow service objectstorage-${var.region} to manage object-family in tenancy",
  ]
}

resource "oci_identity_policy" "service_connector" {
  name           = "${var.vcn_name}-service-connector-policy"
  description    = "This policy is created for the service connector. Connection Source: objectStorage"
  compartment_id = var.tenancy_ocid
  statements = [
    "Allow any-user to manage objects in compartment id ${var.tenancy_ocid} where all {request.principal.type='serviceconnector', target.bucket.name='${oci_objectstorage_bucket.this.name}', request.principal.compartment.id='${var.tenancy_ocid}'}",
  ]
}

##################################
# Object storage &
# Log Objects Handling
##################################

resource "oci_objectstorage_bucket" "this" {
  #Required
  compartment_id = var.tenancy_ocid
  name = var.bucket_name
  namespace = var.bucket_namespace
  versioning = var.bucket_versioning
  access_type    = "NoPublicAccess"
}

resource "oci_objectstorage_object_lifecycle_policy" "this" {
  bucket = oci_objectstorage_bucket.this.name
  namespace = oci_objectstorage_bucket.this.namespace

  rules {
    action = "DELETE"
    is_enabled = "true"
    name = "${var.vcn_name}-logs-lifecycle-policy"
    time_amount = "3"
    time_unit = "DAYS"
    target = "objects"
  }
}


##################################
# Log Group &
# Service Connector Hub
################################## 

resource "oci_sch_service_connector" "this" {
  compartment_id = var.tenancy_ocid
  description    = "Connect the Logging group with created S3 bucket to funnel flowlogs for collection"
  display_name   = "${var.vcn_name}-flow-logs-connector"

  source {
    kind = "logging"

    dynamic log_sources {
      for_each = local.oci_log_ids
      
      content {
        compartment_id = var.tenancy_ocid
        log_group_id   = var.oci_log_group_id
        log_id         = log_sources.value
      }
    }
  }

  target {
    kind      = "objectStorage"
    bucket = oci_objectstorage_bucket.this.name
    batch_rollover_size_in_mbs = "10"
    batch_rollover_time_in_ms  = "60000"
  }

  state = "ACTIVE"
}