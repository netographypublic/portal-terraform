variable "region" {
  type        = string
  description = "Oracle region for resource deployment"
}

variable "pub_key_value" {
  description = "This uploads an API signing key for the created user."
  type = string
  default = ""
}

variable "vcn_name" {
  description = "VCN name to be added as prefix to created resources"
  type        = string
  default     = ""
}

variable "tenancy_ocid" {
  description = "Tenancy OCID for Oracle account"
  type        = string
  default     = ""
}

variable "oci_log_group_id" {
  description = "OCI Log Group name "
  type        = string
  default     = ""
}

variable "oci_log_ids" {
  description = "OCI Log name "
  type        = list(string)
  default     = [""]
}

variable "bucket_name" {
  description = "Name of the created OCI Bucket"
  type        = string
  default     = ""
}

variable "bucket_namespace" {
  description = "Namespace where the OCI Bucket Name resides"
  type        = string
  default     = ""
}

variable "bucket_versioning" {
  description = "Opytion to version objects in the OCI Bucket"
  type        = string
  default     = "Enabled"
}