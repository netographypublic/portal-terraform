output "bucket_name" {
  value = oci_objectstorage_bucket.this.name
}

output "user_ocid" {
  value = oci_identity_user.this.id
}