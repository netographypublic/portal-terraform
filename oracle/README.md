# Netography Portal Terraform Oracle Integration

Terraform module for Netography Portal Integration.
Enables customers using terraform to directly provision resources for flowlog collection in their Oracle cloud environment and add required cloud provider configuration to the Netography portal in one step.

## Usage

```hcl
module "flowlog" {
 source            = "../module/oci-flowlogs"
  region           = var.region
  pub_key_value    = chomp(file("scripts/public_key.pem"))
  vcn_name         = var.vcn_name
  tenancy_ocid     = var.tenancy_ocid
  oci_log_group_id = var.oci_log_group_id
  oci_log_ids      = var.oci_log_ids
  bucket_name      = var.bucket_name
  bucket_namespace = var.account_namespace
```

## Example

- [Provision Flowlog resources and Authenticate via Netography API to add cloud provider](https://gitlab.com/netographypublic/portal-terraform/-/tree/main/oracle/example)

## Requirements

| Name | Version |
|------|---------|
| <a name="x"></a> [terraform](#requirement\_terraform) | >= 1.1.6 |
| <a name="x"></a> [oracle](#requirement\_oracle_) | >= 4.74.0 |
| <a name="x"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_python) | >= 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [oracle](#provider\_oracle) | >= 4.74.0 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |

## Resources Created

| Name | Type |
|------|------|
| [oci_identity_user.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_user) | resource |
| [oci_identity_api_key.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_api_key) | resource |
| [oci_identity_user_group_membership.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_user_group_membership) | resource |
| [oci_identity_group.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_group) | resource |
| [oci_identity_policy.group](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_policy) | resource |
| [oci_identity_policy.service_connector](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_policy) | resource |
| [oci_objectstorage_bucket.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/objectstorage) | resource |
| [oci_objectstorage_object_lifecycle_policy.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/objectstorage_object_lifecycle_policy) | resource |
| [oci_sch_service_connector.this](https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/sch_service_connector) | resource |

## Inputs

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| <a name="x"></a> [region](#x) | The Oracle region of the VCN | `string` | yes |
| <a name="x"></a> [vcn\_name](#x) | The name of the Oracle VCN to be be logged | `string` | yes |
| <a name="x"></a> [tenancy\_ocid](#x) | The Tenancy ID the Oracle project to be worked on | `string` | yes |
| <a name="x"></a> [oci\_log\_\_id](#x) | The ID of the Oracle Log  to be utillized  | `string` | yes |
| <a name="x"></a> [oci\_log\_group\_id](#x) | The ID of the Oracle Log Group to be utillized | `string` | yes |
| <a name="x"></a> [bucket\_\_name](#x) |The name of the Oracle Object storage to be created | `string` | yes |
| <a name="x"></a> [bucket\_namespace](#x) | The namespace where the Oracle Object storage will be created | `string` | yes |



## Outputs

| Name | Description |
|------|-------------|
| <a name="x"></a> [user\_ocid](#x) | The ID of the Oracle User created |
| <a name="x"></a> [bucket\_name](#x) | The name of the Oracle Object storage created |
| <a name="x"></a> [data\_json](#x) | The response of the HTTP POST Request in JSON format |


## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
