############################################################
# Local exec to use dummy data to create an API key -
# - for user via the Netography Portal API
############################################################

resource "null_resource" "get_public_key" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_pubkey.py"
    environment = {
      PROVIDER_NAME = var.portal_cloud_provider_name
    }
  }
  depends_on = [
    null_resource.get_bearer_token
  ]
}

####################################################
# Create Resources to enable flowlog collection
# Will create User, Group, Bucket, Service connector
####################################################

module "flowlogs" {
  source           = "../module/oci-flowlogs"
  region           = var.region
  pub_key_value    = chomp(file("scripts/public_key.pem"))
  vcn_name         = var.vcn_name
  tenancy_ocid     = var.tenancy_ocid
  oci_log_group_id = var.oci_log_group_id
  oci_log_ids      = var.oci_log_ids
  bucket_name      = var.bucket_name
  bucket_namespace = var.account_namespace

  depends_on = [
    null_resource.get_public_key
  ]
}


############################################################
# Authenticate to the Netography API via local -
# - shell to get a bearer token
############################################################

resource "null_resource" "get_bearer_token" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_token.py"
    environment = {
      CUSTOMER_USERNAME = var.customer_username
      CUSTOMER_PASSWORD = var.customer_password
    }
  }
}

############################################################
# Send PUT request using token to Netography Portal API, - 
# -  to update cloud provider for flow collection
############################################################

data "http" "api_call" {
  depends_on = [
    module.flowlogs
  ]

  provider = http-full
  url      = "https://api.netography.com/api/v1/vpc/${chomp(file("scripts/vpc_id.txt"))}"
  method   = "PUT"
  request_headers = {
    content-type  = "application/json"
    Authorization = "Bearer ${chomp(file("scripts/bearer_token.txt"))}"
  }
  request_body = jsonencode(
    {
      "name" : "${var.portal_cloud_provider_name}",
      "enabled" : true,
      "samplerate" : 1,
      "flowtype" : "oracle",
      "flowresource" : "cos",
      "region" : "${var.region}",
      "userid" : "${module.flowlogs.user_ocid}",
      "bucket" : "${module.flowlogs.bucket_name}",
      "tenancy" : "${var.tenancy_ocid}",
      "prefix" : ""
    }
  )
}