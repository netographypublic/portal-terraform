#!/usr/bin/env python3
import requests
import os

provider_name = os.environ["PROVIDER_NAME"]
print(provider_name)


def get_token():
    with open("scripts/bearer_token.txt", "r") as file:
        return file.read()


def main():
    token = get_token()
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {token}",
    }
    # Dummy data to generate public pem key
    body = {
        "name": provider_name,
        "samplerate": 1,
        "enabled": True,
        "flowtype": "oracle",
        "flowresource": "cos",
        "region": "us-ashburn-1",
        "userid": "XXXXXXXXXXXXXXXX",
        "tenancy": "XXXXXXXXXXXXXXXXXXXXXXX",
        "bucket": "XXXXXXXXXXXXXXXXXXXXXXXXX",
        "prefix": "",
    }

    resp = requests.post(
        "https://api.netography.com/api/v1/vpc",
        json=body,
        headers=headers,
    )
    vpc_id = resp.json()["data"][0]["id"]
    pub_key = resp.json()["data"][0]["publickey"]
    # Write api public key to public_key.pem file
    with open("scripts/public_key.pem", "w") as file:
        file.write(pub_key)
    with open("scripts/vpc_id.txt", "w") as file:
        file.write(vpc_id)


if __name__ == "__main__":
    main()
