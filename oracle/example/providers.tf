##########################
# PROVIDER CONFIGURATION
##########################

provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
  region           = var.region
}

provider "http-full" {}

##########################
# TERRAFORM CONFIGURATION
##########################

terraform {
  required_providers {
    oci = {
      source = "oracle/oci"
    }
    http-full = {
      source  = "salrashid123/http-full"
      version = "1.2.3"
    }
  }
  required_version = ">= 0.13"
}
