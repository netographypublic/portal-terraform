variable "region" {
  type        = string
  description = "Oracle region for resource deployment"
}

variable "vcn_name" {
  description = "VCN name to be added as prefix to created resources"
  type        = string
}

variable "user_ocid" {
  description = "User OCID for Oracle account"
  type        = string
}

variable "fingerprint" {
  description = "fingerprint for user api key"
  type        = string
}

variable "private_key_path" {
  description = "path to private api key on local machine"
  type        = string
}

variable "tenancy_ocid" {
  description = "Tenancy OCID for Oracle account"
  type        = string
}

variable "oci_log_group_id" {
  description = "OCI Log Group name "
  type        = string
}

variable "oci_log_ids" {
  description = "OCI Log name "
  type        = list(string)
}

variable "bucket_name" {
  description = "Name of the created OCI Bucket"
  type        = string
}

variable "account_namespace" {
  description = "Unique Namespace designated to account tenant during creation"
  type        = string
}


variable "portal_cloud_provider_name" {
  type        = string
  description = "Name of the cloud provider account to add to the Netography portal"
}

variable "customer_username" {
  type        = string
  description = "the authenticated username of the Netography portal customer"
}

variable "customer_password" {
  type        = string
  description = "the authenticated password of the Netography portal customer"
}