variable "s3_bucket_name" {
  type        = string
  description = "Name of s3 bucket to be created"
}

variable "block_public_acls" {
  description = "Whether Amazon S3 should block public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "block_public_policy" {
  description = "Whether Amazon S3 should block public bucket policies for this bucket."
  type        = bool
  default     = true
}

variable "ignore_public_acls" {
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "restrict_public_buckets" {
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
  type        = bool
  default     = true
}

variable "flow_log_destination_type" {
  description = "Type of flow log destination. Can be s3 or cloud-watch-logs."
  type        = string
  default     = "s3"
}


variable "flow_log_traffic_type" {
  description = "The type of traffic to capture. Valid values: ACCEPT, REJECT, ALL."
  type        = string
  default     = "ALL"
}

variable "flow_log_log_format" {
  description = "The fields to include in the flow log record, in the order in which they should appear."
  type        = string
  default     = "$${account-id} $${action} $${az-id} $${bytes} $${dstaddr} $${dstport} $${end} $${flow-direction} $${instance-id} $${vpc-id} $${version} $${type} $${traffic-path} $${tcp-flags} $${sublocation-id} $${sublocation-type} $${subnet-id} $${srcport} $${srcaddr}  $${start} $${region} $${protocol} $${pkt-srcaddr} $${pkt-src-aws-service} $${pkt-dstaddr} $${pkt-dst-aws-service} $${packets} $${log-status} $${interface-id}"
}

variable "flow_log_file_format" {
  description = "(Optional) The format for the flow log. Valid values: `plain-text`, `parquet`."
  type        = string
  default     = "plain-text"
}

variable "vpc_id" {
  description = "The id of the VPC that will flowlogs will be created for"
  type        = string
  default     = ""
}

variable "flow_log_max_aggregation_interval" {
  description = "The maximum interval of time during which a flow of packets is captured and aggregated into a flow log record. Valid Values: `60` seconds or `600` seconds."
  type        = number
  default     = 60
}

variable "force_destroy" {
  type        = bool
  description = "Enables a terraform managed resource to be destroyed"
  default     = true
}

variable "sns_topic_name" {
  description = "SNS topic name"
  type        = string
  default     = "vpc-flowlog-collection-topic"
}

variable "sqs_queue_name" {
  description = "SQS topic name"
  type        = string
  default     = "vpc-flowlog-collection-queue"
}

variable "vpc_name" {
  description = "VPC topic name to be added as prefix to created resources"
  type        = string
  default     = ""
}