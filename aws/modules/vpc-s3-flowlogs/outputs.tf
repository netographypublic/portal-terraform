output "access_key_id" {
  value       = aws_iam_access_key.this.id
  description = "The access key ID"
}

output "secret_access_key" {
  sensitive   = true
  value       = aws_iam_access_key.this.secret
  description = "The secret access key. NOTE: This will be written to the state file in plain-text"
}

output "bucket_name" {
  description = "The name of the S3 bucket that will be created"
  value       = aws_s3_bucket.this.id
}

output "bucket_arn" {
  description = "The ARN of the S3 bucket that will be created"
  value       = aws_s3_bucket.this.arn
}

output "queue_url" {
  description = "The URL of the SQS Queue that will be created"
  value       = aws_sqs_queue.this.url
}

output "bucket_region" {
  description = "The region that the S3 Bucket that is created in"
  value       = aws_s3_bucket.this.region
}
