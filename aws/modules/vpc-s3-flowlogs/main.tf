##################################################
# IAM Role & Policy
# Create an IAM User with access s3 & sqs queue
# Define bucket policy for s3_bucket
##################################################

resource "aws_iam_user" "this" {
  name          = "${var.vpc_name}-flowlog-collector-user"
  force_destroy = var.force_destroy
}

resource "aws_iam_access_key" "this" {
  user = aws_iam_user.this.name
}

resource "aws_iam_user_policy_attachment" "this" {
  lifecycle {
    create_before_destroy = true
  }
  user       = aws_iam_user.this.name
  policy_arn = aws_iam_policy.user_policy.arn
}

resource "aws_iam_policy" "user_policy" {
  name        = "${var.vpc_name}-flowlog-collector-policy"
  description = "A test policy"
  policy      = data.aws_iam_policy_document.user_policy.json
}

data "aws_iam_policy_document" "user_policy" {
  depends_on = [
    aws_sqs_queue.this
  ]
  statement {
    sid    = "FlowCollectorUser"
    effect = "Allow"
    actions = [
      "s3:DeleteObject",
      "s3:GetObject",
      "s3:ListBucket",
      "sqs:DeleteMessage",
      "sqs:DeleteMessageBatch",
      "sqs:DeleteQueue",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ListDeadLetterSourceQueues",
      "sqs:ListQueueTags",
      "sqs:ReceiveMessage",
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*",
      "${aws_sqs_queue.this.arn}",
    ]
  }
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid = "AWSLogDeliveryWrite"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:PutObject"]

    resources = ["arn:aws:s3:::${var.s3_bucket_name}/AWSLogs/*"]
  }
  statement {
    sid = "AWSLogDeliveryAclCheck"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:GetBucketAcl"]

    resources = ["arn:aws:s3:::${var.s3_bucket_name}"]
  }
}


####################################################
# S3 Bucket
# Create S3 bucket with event notifications to SNS
# Set force_destroy=false to disable api-termination 
####################################################
resource "aws_s3_bucket" "this" {
  bucket              = var.s3_bucket_name
  force_destroy       = var.force_destroy
}

  # Bucket policies
resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.bucket_policy.json
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.id

  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

resource "aws_s3_bucket_notification" "this" {
  bucket = aws_s3_bucket.this.id

  topic {
    topic_arn     = aws_sns_topic.this.arn
    events        = ["s3:ObjectCreated:*"]
  }

  depends_on = [
    aws_sns_topic.this
  ]
}


####################################################
# SQS Queues & SNS Topics
# SQS Queue to poll SNS topic for newly created -
# - objects in s3 bucket
####################################################

resource "aws_sns_topic" "this" {
  name = var.sns_topic_name
  policy = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": { "Service": "s3.amazonaws.com" },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:${var.sns_topic_name}",
        "Condition":{
            "ArnLike":{"aws:SourceArn":"${aws_s3_bucket.this.arn}"}
        }
    }]
}
POLICY
}

resource "aws_sqs_queue" "this" {
  name                       = var.sqs_queue_name
  visibility_timeout_seconds = 30
  max_message_size           = 262144   #256KB
  message_retention_seconds  = 345600   #4days 
  receive_wait_time_seconds  = 0
}

resource "aws_sqs_queue_policy" "policy" {
  queue_url = aws_sqs_queue.this.url

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Id": "PushMessageToSQSPolicy",
    "Statement": [
      {
        "Sid": "allow-sns-to-send-message-to-sqs",
        "Effect": "Allow",
        "Principal": {
          "AWS": "*"
        },
        "Action": "sqs:SendMessage",
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "aws:SourceArn": "arn:aws:s3:::${var.s3_bucket_name}"
          }
        }
      }
    ]
  }
  EOF
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  topic_arn = aws_sns_topic.this.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.this.arn
}

############################################
# Flow Log
############################################

resource "aws_flow_log" "this" {

  log_destination_type     = var.flow_log_destination_type
  log_destination          = aws_s3_bucket.this.arn
  log_format               = var.flow_log_log_format
  traffic_type             = var.flow_log_traffic_type
  vpc_id                   = var.vpc_id
  max_aggregation_interval = var.flow_log_max_aggregation_interval

  destination_options {
    file_format = var.flow_log_file_format
  }
}