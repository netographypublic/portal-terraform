#!/usr/bin/env python3
import requests
import os

customer_username = os.environ["CUSTOMER_USERNAME"]
customer_password = os.environ["CUSTOMER_PASSWORD"]


def main():
    params = {
        "username": customer_username,
        "password": customer_password
    }
    resp = requests.post(
        'https://api.netography.com/api/auth/bearer/token', params
    )
    auth_token = resp.json()["access_token"]
    # Write authentication token to bearer_token.txt file
    with open("scripts/bearer_token.txt", "w") as file:
        file.write(auth_token)


if __name__ == '__main__':
    main()
