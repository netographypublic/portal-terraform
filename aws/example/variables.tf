variable "region" {
  type        = string
  description = "AWS region for recource deployment"
}

variable "vpc_name" {
  type        = string
  description = "Name of the vpc"
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of the S3 bucket to be created"
}

variable "sns_topic_name" {
  type        = string
  description = "Name of the SNS Topic to be created"
  default     = "vpc-flowlog-collection-topic"
}

variable "sqs_queue_name" {
  type        = string
  description = "Name of the SQS Queue to be created"
  default     = "vpc-flowlog-collection-queue"
}

variable "portal_cloud_provider_name" {
  type        = string
  description = "Name of the cloud provider account to add to the Netography portal"
}

variable "folder_prefix" {
  type        = string
  description = "Folder path inside the S3 bucket to store and retrieve logs from"
  default     = ""
}

variable "customer_username" {
  type        = string
  description = "the auntheticated username of the Netography portal customer"
}

variable "customer_password" {
  type        = string
  description = "the auntheticated password of the Netography portal customer"
}
