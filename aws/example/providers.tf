##########################
# PROVIDER CONFIGURATION
##########################

provider "aws" {
  region = var.region
  default_tags {
    ## Global Tags, tags here will be used for all created resources
    tags = {
      Automation  = "Terraform"
      Environment = "production"
    }
  }
}

provider "http-full" {}

##########################
# TERRAFORM CONFIGURATION

##########################

terraform {
  required_version = ">=1.1.6"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.6"
    }
    http-full = {
      source  = "salrashid123/http-full"
      version = "1.2.3"
    }
  }
}


# USE THIS TO STORE YOUR TF STATE REMOTELY
# NOTE: S3 BUCKET AND DYNAMO-DB TABLE MUST ALREADY EXIST

# terraform {
#   backend "s3" {
#     bucket         = "company-terraform-backend"
#     key            = "vpc-s3/flowlogs/"
#     region         = "us-east-1" ##hardcode your region here
#     dynamodb_table = "terraform_state_lock"
#     encrypt        = true
#   }
# }