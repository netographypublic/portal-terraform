####################################################
# Data Source VPC 
####################################################
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.vpc_name}"]
  }
}

####################################################
# Create Resources to enable flowlog collection
####################################################
module "flowlog" {
  source         = "../modules/vpc-s3-flowlogs"
  vpc_id         = data.aws_vpc.vpc.id
  vpc_name       = var.vpc_name
  s3_bucket_name = var.s3_bucket_name
  # sns_topic_name = var.sns_topic_name     #Optional, NOTE: if specified,module will auto-add VPC prefix
  # sqs_queue_name = var.sqs_queue_name     ##Optional, NOTE: if specified,module will auto-add VPC prefix
}

############################################################
# Authenticate to the Netography API via local -
# - shell to get a bearer token
############################################################
resource "null_resource" "get_bearer_token" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/get_token.py"
    environment = {
      CUSTOMER_USERNAME = var.customer_username
      CUSTOMER_PASSWORD = var.customer_password
    }
  }
}

############################################################
# Send POST request using bearer token to Netography API, - 
# -  add cloud provider to portal for flow collection
############################################################
data "http" "api_call" {
  depends_on = [
    module.flowlog
  ]

  provider = http-full
  url      = "https://api.netography.com/api/v1/vpc"
  method   = "POST"
  request_headers = {
    content-type  = "application/json"
    Authorization = "Bearer ${chomp(file("scripts/bearer_token.txt"))}"
  }
  request_body = jsonencode(
    {
      "name" : "${var.portal_cloud_provider_name}",
      "enabled" : true,
      "samplerate" : 1,
      "flowtype" : "aws",
      "flowresource" : "s3",
      "region" : "${var.region}",
      "accountid" : "${data.aws_vpc.vpc.owner_id}",
      "accesskeyid" : "${module.flowlog.access_key_id}",
      "accesssecret" : "${module.flowlog.secret_access_key}",
      "bucket" : "${module.flowlog.bucket_name}",
      "bucketregion" : "${module.flowlog.bucket_region}",
      "prefix" : "${var.folder_prefix}", #Optional, specify path if you will be using one bucket for multiple VPC flowlogs collection
      "sqsurl" : "${module.flowlog.queue_url}"
    }
  )
}


