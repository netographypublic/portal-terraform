# Netography Portal Terraform Integration

Terraform module for Netography Portal Integration. 
Enables customers using terraform to directly provision resources for flowlog collection in their AWS cloud environment and add required cloud provider configuration to the Netography portal in one step.

## Usage

```hcl
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.vpc_name}"]
  }
}

module "flowlog" {
    
  source         = "../modules/vpc-s3-flowlogs"
  vpc_id         = data.aws_vpc.vpc.id
  s3_bucket_name = var.s3_bucket_name
  vpc_name       = var.vpc_name
```

## Example

- [Provision Flowlog resources and Authenticate via Netography API to add cloud provider](https://gitlab.com/netographypublic/portal-terraform/-/tree/main/aws/example)


## Disable API Termination

Because production grade infrastucture is involved, disable api termination in order for resources created to not be deletable via terraform for extra caution

```hcl
# The IAM user and S3 bucket which stores logs can not be terminated using terraform

module "flowlog" {
    
  source         = "../modules/vpc-s3-flowlogs"
  force_destroy = "false"
  # ... omitted
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.63 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |
| <a name="x"></a> [python](#requirement\_http) | >= 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.63 |
| <a name="requirement_http"></a> [http-full](#requirement\_http) | >= 1.2.3 |

## Data Sources & Resources Created

| Name | Type |
|------|------|
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [aws_iam_user.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_s3_bucket.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_sqs_queue.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue) | resource |
| [aws_sns_topic.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_flow_log.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/flow_log) | resource |

## Inputs

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| <a name="x"></a> [force\_destroy](#x) | Enables a terraform managed resource to be destroyed | `bool` | no |
| <a name="x"></a> [vpc\_id](#x) | The id of the VPC that will flowlogs will be created for| `bool` | no |
| <a name="x"></a> [vpc\_name](#x) | The name of the VPC that will flowlogs will be created for  | `number` | yes |
| <a name="x"></a> [s3\_bucket\_name](#x) | The name of the created Amazon S3 bucket| `string` | yes |
| <a name="x"></a> [sns\_topic\_name](#x) | The name of the created Amazon SNS topic | `bool` | no |
| <a name="x"></a> [sqs\_queue\_name](#x) | The name of the created Amazon SQS queue | `string` | no |
| <a name="x"></a> [flow\_log\_max\_aggregation\_interval](#x) | The maximum interval of time during which a flow of packets is captured and aggregated into a flow log record. Valid Values: `60` seconds or `600` seconds. | `string` | no |


## Outputs

| Name | Description |
|------|-------------|
| <a name="x"></a> [account\_id](#x) | The id of the AWS account |
| <a name="x"></a> [vpc\_region](#x) | The region of the Virtual Private Cloud generating flow logs |
| <a name="x"></a> [s3\_bucket\_region](#x) | The region of the created Amazon S3 bucket |
| <a name="x"></a> [iam\_user\_access\_key](#x) |The IAM user Access Key Id |
| <a name="x"></a> [iam\_user\_secret\_key](#x) | The IAM user Secret Access Key |
| <a name="x"></a> [s3\_bucket\_name](#x) | The name of the created Amazon S3 bucket |
| <a name="x"></a> [sqs\_queue\_url](#x) | The URL for the created Amazon SQS queue |
| <a name="x"></a> [data\_json](#x) | The response of the HTTP POST Request in JSON format |


## License

Apache 2 Licensed. See [LICENSE](https://gitlab.com/netographypublic/portal-terraform/-/blob/main/LICENSE) for full details.
